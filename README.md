Welcome to Warfare!
=========
This is the Git repository project page for **Warfare** on GitLab. **Warfare** is currenlty in active development.
**Warfare** is a team based multiplayer modern military shooter game.

Prerequisites
-------------
**Warfare** requires the following third party software:

### Windows
- [Unreal Engine 5](https://www.unrealengine.com/en-US/download): Used to develop the **Warfare** project.
- [Visual Studio 2022](https://visualstudio.microsoft.com/downloads/): Used to compile the **Warfare** project.
- [Git](https://git-scm.com/downloads): Used to clone/fork the **Warfare** project. 

Getting Started
---------------
**Warfare** is currently using Unreal Engine 5.4. This project only includes the project source, the Content folder is ignored.    

### Windows
1. Download or fork/clone the project to a folder on your computer.
2. Open the project in file explorer.
3. Run the Warfare.uproject file to generate a Visual Studio solution file. 
4. Open the Warfare.sln file to compile the project source.

Contributions
-------------
Pull requests for features not specified within the roadmap will be ignored. 