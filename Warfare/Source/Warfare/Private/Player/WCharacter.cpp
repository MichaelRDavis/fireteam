// Fill out your copyright notice in the Description page of Project Settings.

#include "Player/WCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

AWCharacter::AWCharacter()
{
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(0.0f, 0.0f, BaseEyeHeight));
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	ArmsMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	ArmsMesh->SetupAttachment(FirstPersonCameraComponent);
	ArmsMesh->SetGenerateOverlapEvents(false);
	ArmsMesh->AlwaysLoadOnClient = true;
	ArmsMesh->AlwaysLoadOnServer = true;
	ArmsMesh->bOnlyOwnerSee = true;
	ArmsMesh->bOwnerNoSee = false;
	ArmsMesh->bReceivesDecals = false;
	ArmsMesh->CastShadow = false;
	ArmsMesh->bCastDynamicShadow = false;
	ArmsMesh->bAffectDynamicIndirectLighting = false;
	ArmsMesh->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;
	ArmsMesh->PrimaryComponentTick.TickGroup = TG_PrePhysics;

	GetMesh()->bOnlyOwnerSee = false;
	GetMesh()->bOwnerNoSee = true;
	GetMesh()->bReceivesDecals = false;
}

void AWCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// Limit pitch when walking or falling
		const bool bLimitRotation = (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling());

		// Find out which way is forward
		const FRotator Rotation = bLimitRotation ? GetActorRotation() : GetControlRotation();
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);

		// Add movement in forward direction
		AddMovementInput(Direction, Value);
	}
}

void AWCharacter::StrafeRight(float Value)
{
	if (Value != 0.0f)
	{
		// Find out which way is right
		const FQuat Rotation = GetActorQuat();
		const FVector Direction = FQuatRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);

		// Add movement in right direction
		AddMovementInput(Direction, Value);
	}
}