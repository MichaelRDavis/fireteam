// Fill out your copyright notice in the Description page of Project Settings.

#include "Player/WPlayerController.h"
#include "Player/WCharacter.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"

AWPlayerController::AWPlayerController()
{

}

void AWPlayerController::BeginPlay()
{
	Super::BeginPlay();

	// Get the enhanced input subsystem
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		// Add the mapping context so we get controls
		Subsystem->AddMappingContext(PlayerIMC, 0);
	}
}

void AWPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent))
	{
		// Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AWPlayerController::Move);
		// Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &AWPlayerController::Look);
		// Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &AWPlayerController::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &AWPlayerController::StopJumping);
	}
}

void AWPlayerController::SetPawn(APawn* InPawn)
{
	AController::SetPawn(InPawn);

	WCharacter = Cast<AWCharacter>(InPawn);
}

void AWPlayerController::Move(const FInputActionValue& Value)
{
	// Input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (WCharacter != nullptr)
	{
		WCharacter->MoveForward(MovementVector.Y);
		WCharacter->StrafeRight(MovementVector.X);
	}
}

void AWPlayerController::Look(const FInputActionValue& Value)
{
	// Input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	AddYawInput(LookAxisVector.X);
	AddPitchInput(LookAxisVector.Y);
}

void AWPlayerController::Jump()
{
	if (WCharacter != nullptr)
	{
		WCharacter->Jump();
	}
}

void AWPlayerController::StopJumping()
{
	if (WCharacter != nullptr)
	{
		WCharacter->StopJumping();
	}
}
